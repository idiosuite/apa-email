<?php

class BillingTableMarkupProvider
{
	private $htmlTemplateBasePath;
	
	public function __construct($htmlTemplateBasePath)
	{
		$this->htmlTemplateBasePath = $htmlTemplateBasePath;
	}

	public function getRows()
	{
		return [
			['table-head', $this->getTableHeaderMarkup()],
			['table-rows', $this->getTableRowsMarkup()]
		];
	}

	private function getTableHeaderMarkup()
	{
		$template = $this->read('_billing.table.head.html');
		
		return vsprintf(
			$template, 
			(new TableCsv)->getHeaders()
		);
	}

	private function getTableRowsMarkup()
	{
		$template = $this->read('_billing.table.row.html');
		
		$markup = '';
		
		foreach ((new TableCsv)->getRows() as $row) {
			$markup.= vsprintf($template, $row);
		}
		
		return $markup;
	}

	private function read($fileName)
	{
		return file_get_contents($this->htmlTemplateBasePath . '/' . $fileName);
	}
}