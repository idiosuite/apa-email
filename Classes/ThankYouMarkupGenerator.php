<?php

class ThankYouMarkupGenerator extends AbstractMarkupGenerator
{
	protected $bodyPartial = '_message.html';

	protected function replaceBodyTokens($markupTemplate)
	{
		return $this->replaceTokens([
			['body', $this->getBody()]
		], $markupTemplate);
	}

	private function getBody()
	{
		return (new TxtToHtmlConverter(
			$this->htmlTemplateBasePath . '/../txt/thanks.txt'
		))->get();
	}
}