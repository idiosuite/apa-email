<?php

class HeadingCsv
{
	public function getHeaders()
	{
		return ['key', 'value'];
	}

	public function getRows()
	{
		// This is just a stub.
		// This data will most likely come from a database
		return [
			["heading-place", "Pessano con Bornago"],
			["heading-date", date('d.m.Y')],
		];
	}
}