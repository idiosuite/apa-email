<?php

class DictionaryBillingCsv
{
	public function getHeaders()
	{
		// This is just a stub.
		// This data will be actually loaded from /csv/dictionary.billing.csv

		return ['key', 'value'];
	}

	public function getRows()
	{
		// This is just a stub.
		// This data will be loaded from /csv/dictionary.billing.csv

		return [
			["intro-greetings","Caro Cliente,"],
			["intro-body", "con la presente Le ricordiamo che, il {billing-date}, sono giunte a scadenza le fatture sotto elencate che ci risultano tutt'ora insolute.<br/><br/>Siamo pienamente consapevoli della difficile situazione che ci ha tutti travolto, ma confidiamo riusciate quanto prima ad assolvere gli impegni presi.<br/>"],
			["payment-instructions", "Per effettuare un bonifico a saldo degli insoluti, potete utilizzare il seguente IBAN"],
			["payment-iban","IT00 Y 00000 00000 000000000000"],
			["payment-bank","INTESA S.PAOLO Filiale di Gorgonzola"],
			["payment-closure","Qualora, nel frattempo, abbiate provveduto a saldare le fatture scadute, vogliate ritenere nulla la presente."],
			["greetings","Cordiali saluti."],
			["signature","A.P.A. S.p.A."],
		];
	}
}