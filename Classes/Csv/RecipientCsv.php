<?php

class RecipientCsv
{
	public function getHeaders()
	{
		return ['key', 'value'];
	}

	public function getRows()
	{
		// This is just a stub.
		// This data will most likely come from a database
		return [
			["recipient-name", "PIPPO PAPPO S.N.C."],
			["recipient-address", "VIA GARIBALDI 1"],
			["recipient-city", "12345 CASALMAGGIORE - CR"],
			["recipient-email", "info@pippopappo.net"],
			["billing-date", '31 marzo 2020'],
		];
	}
}