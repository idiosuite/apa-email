<?php

abstract class AbstractMarkupGenerator
{
	protected $htmlTemplateBasePath;
	protected $bodyPartial;

	public function __construct($htmlTemplateBasePath)
	{
		$this->htmlTemplateBasePath = $htmlTemplateBasePath;
	}

	public function output($pathToFile)
	{
		file_put_contents(
			$pathToFile, 
			$this->generateMarkup()
		);
	}

	public function generateMarkup()
	{
		$markupTemplate = file_get_contents($this->htmlTemplateBasePath . '/markup.html');
		
		$markupTemplate = $this->loadRecipientPartial($markupTemplate);
		$markupTemplate = $this->loadHeadingPartial($markupTemplate);
		$markupTemplate = $this->loadBodyPartial($this->bodyPartial, $markupTemplate);
		
		$markupTemplate = $this->replaceBodyTokens($markupTemplate);
		$markupTemplate = $this->replaceRecipientTokens($markupTemplate);
		$markupTemplate = $this->replaceHeadingTokens($markupTemplate);
		
		return $markupTemplate;
	}

	abstract protected function replaceBodyTokens($markupTemplate);

	protected function loadRecipientPartial($markupTemplate)
	{
		return $this->loadPartial(
			'{partial-recipient}', 
			'_recipient.html', 
			$markupTemplate
		);
	}

	protected function loadHeadingPartial($markupTemplate)
	{
		return $this->loadPartial(
			'{partial-heading}', 
			'_heading.html', 
			$markupTemplate
		);
	}

	protected function loadBodyPartial($filename, $markupTemplate)
	{
		return $this->loadPartial(
			'{partial-body}', 
			$filename, 
			$markupTemplate
		);
	}

	protected function replaceRecipientTokens($markupTemplate)
	{
		return $this->replaceTokens(
			(new RecipientCsv)->getRows(), 
			$markupTemplate
		);
	}

	protected function replaceHeadingTokens($markupTemplate)
	{
		return $this->replaceTokens(
			(new HeadingCsv)->getRows(), 
			$markupTemplate
		);
	}

	protected function loadPartial($token, $sourceFileName, $markupTemplate)
	{
		return str_replace(
			$token,
			file_get_contents($this->htmlTemplateBasePath . '/' . $sourceFileName),
			$markupTemplate
		);
	}

	protected function replaceTokens(array $keyValuePairs, $markupTemplate)
	{
		foreach ($keyValuePairs as $row) {
			list ($key, $value) = $row;
			// e.g. replace {recipient-name} with 'PIPPO PAPPO S.N.C.'
			$find = sprintf('{%s}', $key);
			$markupTemplate = str_replace($find, $value, $markupTemplate);
		}

		return $markupTemplate;
	}
}